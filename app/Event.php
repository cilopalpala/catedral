<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = "events";
    
    protected $fillable = ['fecha','titulo','descripcion'];
    
    public function user(){
        return $this->belongsTo('App\User');
    }
    
    public function tags(){
        return $this->belongsToMany('App\Tag');
    }
}

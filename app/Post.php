<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";
    
    protected $fillable = ['titulo','descripcion','contador','user_id','type_id'];
    
    public function images(){
        return $this->hasMany('App\Image');
    }
    public function type(){
        return $this->belongsTo('App\Type');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prayer extends Model
{
    protected $table = "prayers";
    
    protected $fillable = ['titulo','descripcion','fecha','user_id'];
    
    public function user(){
        return $this->belongsTo('App\User');
    }
}

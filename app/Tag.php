<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = "tags";
    
    protected $fillable = ['nombre','descripcion'];
    
    public function events(){
        return $this->belongsToMany('App\Event')->withTimestamps();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = "types";
    
    protected $fillable = ['nombre','descripcion'];
    
    public function posts(){
        return $this->hasMany('App\Post');
    }
}
